#include <iostream>
#include <algorithm>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <utility>

using std::ios_base;
using std::cin;
using std::cout;

using std::srand;
using std::random_shuffle;

using std::vector;

using std::pair;
using std::make_pair;

struct demension {
    int x;
    int y;
    int z;
};

struct demensionScope {
    pair<int, int> xScope;
    pair<int, int> yScope;
    pair<int, int> zScope;
};

class Randomize {
 private:
    bool seedUpdate;
    int startIndex, endIndex;

    demensionScope coorScope;

    vector<demension> arr3Dcoor;

 public:
    Randomize(demensionScope scope, int startPos, int endPos, bool seed);

    int randomN(int from, int to);

    void createRandomize();
    void shuffle();

    void show3D();
};
