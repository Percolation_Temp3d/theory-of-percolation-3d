#include "randomizer.h"

Randomize::Randomize(demensionScope scope, int startPos, int endPos, bool seed = false) {
    seedUpdate = seed;

    coorScope = scope;

    startIndex = startPos;
    endIndex = endPos;

    srand((unsigned)time(NULL));
}

int Randomize::randomN(int from, int to) {
    return rand() % (to - from + 1) + from;
}

void Randomize::shuffle() {
    random_shuffle(&arr3Dcoor[startIndex], &arr3Dcoor[endIndex]);
}

void Randomize::createRandomize() {
    if (seedUpdate) {
        srand((unsigned)time(NULL));
    }

    for (int curI = startIndex; curI < endIndex; curI++) {
        demension point;

        point.x = randomN(coorScope.xScope.first, coorScope.xScope.second);
        point.y = randomN(coorScope.yScope.first, coorScope.yScope.second);
        point.z = randomN(coorScope.zScope.first, coorScope.zScope.second);

        arr3Dcoor.push_back(point);
    }

    shuffle();
}

void Randomize::show3D() {
    for (int curI = startIndex; curI < endIndex; curI++) {
        cout << "(";
        cout << arr3Dcoor[curI].x << ", ";
        cout << arr3Dcoor[curI].y << ", ";
        cout << arr3Dcoor[curI].z << "); ";
    }
}

int main() {
    demensionScope scope = {
        make_pair(0, 100),
        make_pair(0, 100),
        make_pair(200, 300)
    };

    Randomize *rand3D = new Randomize(scope, 0, 1000, true);

    rand3D->createRandomize();
    rand3D->shuffle();
    rand3D->show3D();

    return 0;
}
